/** @type {import('tailwindcss').Config} */
import colors from 'tailwindcss/colors';
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  corePlugins: {
    preflight: false
  },
  theme: {
    extend: {
      colors: {
        amber: colors.amber,
        lime: colors.lime,
        rose: colors.rose,
        orange: colors.orange
      },
      backgroundColor: {
        box: 'var(--bg-light-color)',
        base: 'var(--bg-base-color)',
        bu: 'var(--bg-bu-color)'
      },
      textColor: {
        ba: 'var(--text-base-color)',
        bu: 'var(--text-bu-color)',
        remark: 'var(--text-remark-color)',
        name: 'var(--text-name-color)',
        0: 'var(--text-font-0)',
        1: 'var(--text-font-1)',
        2: 'var(--text-font-2)',
        3: 'var(--text-font-3)',
        4: 'var(--text-font-4)'
      },
      width: {
        '400px': '400px',
        '550px': '550px',
        '850px': '850px',
        '1000px': '1000px'
      }
    }
  },
  plugins: []
};
