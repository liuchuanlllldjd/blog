import { createRouter, createWebHistory } from 'vue-router';
import LayoutPage from '@/views/LayoutPage/LayoutPage.vue';
import { useUserStore } from '@/stores/user';
import { headTabRoute } from './headPages';

const whitelist = ['/login'];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'layout',
      component: LayoutPage,
      redirect: '/home',
      children: [
        ...headTabRoute,
        {
          path: '/personal',
          name: 'personal',
          component: () => import('../views/Personal/PersonalPage.vue')
        },
        {
          path: '/blog/:blogId',
          name: 'blog',
          component: () => import('../views/Home/BlogDetails.vue')
        },
        {
          path: '/CreatorCenter',
          name: 'creatorCenter',
          component: () => import('../views/CreatorCenter/CreatorCenter.vue'),
          redirect: '/CreatorCenter/article',
          children: [
            {
              path: 'article',
              name: 'article',
              component: () =>
                import('../views/CreatorCenter/Article/ArticleCenter.vue')
            },
            {
              path: 'dataCenter',
              name: 'dataCenter',
              component: () =>
                import('../views/CreatorCenter/DataCenter/DataCenter.vue')
            }
          ]
        }
      ]
    },

    {
      path: '/editor',
      name: 'editor',
      component: () => import('../views/Editor/EditorPage.vue')
    },
    {
      path: '/editor/:blogId',
      name: 'editBlog',
      component: () => import('../views/Editor/EditorPage.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/Login/LoginPage.vue')
    }
  ]
});

router.beforeEach((to) => {
  const userStore = useUserStore();
  const token = userStore.aceToken;
  if (to.path !== '/login') {
    userStore.getCurrentUser();
  }
  if (!whitelist.includes(to.path) && !token) {
    return {
      path: '/login'
    };
  }
  return true;
});

export default router;
