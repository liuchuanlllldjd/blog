export const headTabRoute = [
  {
    title: '首页',
    path: '/home',
    name: 'home',
    component: () => import('../views/Home/HomePage.vue')
  },
  {
    title: '帖子',
    path: '/post',
    name: 'post',
    component: () => import('../views/Post/PostPage.vue')
  }
];
