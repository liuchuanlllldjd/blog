import type { IUser } from './user.type';

export interface IAddBlogParams {
  isDraft: boolean;
  title: string;
  content: string;
  blogType: string;
}
export enum IBlogType {
  'FrontEnd' = '1', //前端
  'RearEnd' = '2' //后端
}

export interface IGetBlogListParams {
  current: number;
  pageSize: number;
  blogType?: IBlogType;
}

export interface IGetUserBlogListParams {
  current: number;
  isDraft: boolean;
}

export interface IBlogItem {
  blogId: number;
  content: string;
  title: string;
  blogType: '1' | '2';
  createdAt: string;
  updatedAt: string;
  user?: IUser;
  isDraft: boolean;
}
export interface IGetBlogListRes {
  current: number;
  pageSize: number;
  total: number;
  list: IBlogItem[];
}

export interface IGetBlogDetailParams {
  blogId: number;
}
