export type RegisterParams = {
  username: string;
  password: string;
};

export type LoginParams = {
  username: string;
  password: string;
};

export type LoginResult = {
  token: string;
};

export interface IUser {
  userId: number;
  username: string;
  avatar: string;
  gender: string;
  isDelete: boolean;
  updatedAt: string;
  createdAt: string;
  nickname: string | null;
  email: string | null;
  phone: string | null;
}
