import { http } from '@/utils/http';
import type {
  IAddBlogParams,
  IBlogItem,
  IGetBlogDetailParams,
  IGetBlogListParams,
  IGetBlogListRes,
  IGetUserBlogListParams
} from '../types/blog.type';

/**
 *
 * @param data
 * @returns
 */
export const addBlogApi = (data: IAddBlogParams) => {
  return http({
    url: '/api/blog',
    method: 'POST',
    data
  });
};

export const updateBlogApi = (blogId: number, data: IAddBlogParams) => {
  console.log(blogId, data);
  return http({
    url: `/api/blog/${blogId}`,
    method: 'PATCH',
    data
  });
};

export const deleteBlogApi = (blogId: number) => {
  return http({
    url: `/api/blog/${blogId}`,
    method: 'DELETE'
  });
};

export const getBlogListApi = (params: IGetBlogListParams) => {
  return http<IGetBlogListRes>({
    url: '/api/blog',
    method: 'GET',
    params
  });
};

export const getUserBlogListApi = (params: IGetUserBlogListParams) => {
  return http<IGetBlogListRes>({
    url: '/api/blog/list',
    method: 'GET',
    params
  });
};

export const getBlogDetailApi = (params: IGetBlogDetailParams) => {
  const { blogId, ...others } = params;
  return http<IBlogItem>({
    url: `/api/blog/${blogId}`,
    method: 'GET',
    params: others
  });
};
