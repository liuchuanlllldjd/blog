import { fileURLToPath } from 'node:url';
import COS from 'cos-js-sdk-v5';

import { http } from '@/utils/http';

export const cos = new COS({
  // 配置临时密钥
  getAuthorization: (options, callback) => {
    getSts().then((res) => {
      const { credentials, expiredTime, startTime } = res;
      const { sessionToken, tmpSecretId, tmpSecretKey } = credentials;
      callback({
        TmpSecretId: tmpSecretId,
        TmpSecretKey: tmpSecretKey,
        XCosSecurityToken: sessionToken,
        StartTime: startTime,
        ExpiredTime: expiredTime
      });
    });
  }
});

const config = {
  Bucket: 'blogfile-1311014746',
  Region: 'ap-shanghai'
};

export const addFile = async (filename: string, file: File) => {
  const { Location } = await cos.putObject({
    ...config,
    Key: filename,
    Body: file
  });
  return {
    url: `http://${Location}`,
    filename
  };
};

export const getSts = () => {
  return http({
    url: '/api/cos/sts',
    method: 'POST'
  });
};
