import type { InterConfig } from '../utils/http/request/types';
import { http } from '@/utils/http';
import type { Ref } from 'vue';
import type {
  RegisterParams,
  LoginParams,
  LoginResult
} from '../types/user.type';

/**
 *
 * @param data
 * @param interConfig
 */
export const registerApi = (data: RegisterParams, interConfig: InterConfig) => {
  return http(
    {
      url: '/api/user/register',
      method: 'POST',
      data
    },
    interConfig
  );
};

/**
 * @param data
 * @param interConfig
 */
export const loginApi = (data: LoginParams, interConfig: InterConfig) => {
  return http<LoginResult>(
    {
      url: '/api/auth/login',
      method: 'POST',
      data
    },
    interConfig
  );
};

export const getUserApi = () => {
  return http<LoginResult>({
    url: '/api/user/info',
    method: 'GET'
  });
};
