import type { AxiosResponse, AxiosError } from 'axios';
import Request from './request';
import type { InterConfig, RequestConfig } from './request/types';
import { ElMessage, ElNotification } from 'element-plus';
import router from '@/router';
import { useUserStore } from '@/stores/user';

export interface CResponse<T = any> {
  success: boolean;
  message: string;
  data: T;
}

// 错误处理方案： 错误类型
enum ErrorShowType {
  SILENT = 0,
  WARN_MESSAGE = 1,
  ERROR_MESSAGE = 2,
  NOTIFICATION = 3
}
enum HttpCode {
  OK = 200,
  CREATED = 201,
  NO_CONTENT = 204,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404,
  INTERNAL_SERVER_ERROR = 500
}

export interface ResponseError {
  success: boolean;
  data: any;
  errorCode: HttpCode;
  errorMessage: string;
  showType: ErrorShowType;
  redirectUrl?: 'string';
}

const request = new Request({
  baseURL: import.meta.env.VITE_BASE_API,
  timeout: 1000 * 60 * 5,
  interceptors: {
    // // 请求拦截器
    requestInterceptors: (config) => {
      const userStore = useUserStore();
      const token = userStore.aceToken;
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      return config;
    },
    // // 响应拦截器
    // responseInterceptors: (result: AxiosResponse) => result,
    // //请求错误捕获
    // requestInterceptorsCatch: (config) => config,
    //响应错误捕获
    responseInterceptorsCatch(error: AxiosError<ResponseError>) {
      if (!error.response) {
        ElMessage.error('无响应');
      }
      const data = error.response?.data;
      if (data) {
        const { showType, errorMessage, errorCode } = data;
        switch (showType) {
          case ErrorShowType.SILENT:
            // do nothing
            break;
          case ErrorShowType.WARN_MESSAGE:
            ElMessage.warning(errorMessage);
            break;
          case ErrorShowType.ERROR_MESSAGE:
            ElMessage.error(errorMessage);
            break;
          case ErrorShowType.NOTIFICATION:
            ElNotification({
              title: 'Info',
              message: 'This is an info message',
              type: 'info'
            });
            break;
          default:
            ElMessage.error(errorMessage);
        }
        // 对特殊状态码做处理
        switch (errorCode) {
          case HttpCode.UNAUTHORIZED:
            //未登陆
            router.replace({
              path: '/login'
            });
            break;
          default:
        }
      }
      return Promise.reject(data || { errorMessage: '无响应' });
    }
  }
});

/**
 * @description: 函数的描述
 * @generic D 请求参数
 * @generic T 响应结构
 * @returns {Promise}
 */
export const http = async <R = any>(
  config: RequestConfig<CResponse<R>>,
  interConfig?: InterConfig
) => {
  const res = await request.request<CResponse<R>>(config, interConfig);
  return res?.data;
};
// // 取消请求
// export const cancelRequest = (url: string | string[]) => {
//   return request.cancelRequest(url)
// }
// // 取消全部请求
// export const cancelAllRequest = () => {
//   return request.cancelAllRequest()
// }
