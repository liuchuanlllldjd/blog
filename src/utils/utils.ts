export const debounce = (func: any, wait: number) => {
  let timer: any = null;
  console.log(timer);
  return () => {
    if (timer) clearTimeout(timer);
    timer = setTimeout(func, wait);
  };
};
