import type { InterConfig } from './../utils/http/request/types';
import { ref, computed, watch, reactive } from 'vue';
import { defineStore } from 'pinia';
import { getUserApi, loginApi } from '@/api/user';
import { useRouter } from 'vue-router';
import type { LoginParams, IUser } from '@/types/user.type';

export const useUserStore = defineStore(
  'user',
  () => {
    const router = useRouter();

    /**
     * 当前用户信息
     */
    let currentUser = reactive<IUser>({
      userId: 0,
      username: '',
      avatar: '',
      gender: '',
      isDelete: false,
      nickname: '',
      email: '',
      phone: ''
    });
    const getCurrentUser = async () => {
      const res = await getUserApi();
      currentUser = Object.assign(currentUser, res);
    };

    /**
     * @param ruleForm
     * @param interConfig
     * 登录
     */
    const aceToken = ref<string>();
    const login = async (ruleForm: LoginParams, interConfig: InterConfig) => {
      try {
        const { token = '' } = (await loginApi(ruleForm, interConfig)) || {};
        aceToken.value = token;
        router.replace('/home');
      } catch (error) {
        console.log(error);
      }
    };

    /**
     * 退出登录
     */
    const logout = async () => {
      aceToken.value = '';
      router.replace('/login');
    };

    const activeHeadTab = ref('/home');

    /**
     * 目前正在交互的用户
     */
    const targetUser = ref<IUser>();
    const setTargetUser = (user: IUser) => {
      targetUser.value = user;
    };

    return {
      aceToken,
      currentUser,
      activeHeadTab,
      targetUser,

      login,
      logout,
      getCurrentUser,
      setTargetUser
    };
  },
  { persist: true }
);
