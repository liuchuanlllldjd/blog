import { defineStore } from 'pinia';
import { ref } from 'vue';

import { IBlogType } from '@/types/blog.type';

export const useBlogStore = defineStore('blog', () => {
  const blogType = ref<IBlogType>(IBlogType.FrontEnd);
  const activeName = ref('article');

  return { blogType, activeName };
});
